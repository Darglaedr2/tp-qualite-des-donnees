# TP Qualité des données
## Le Contexte

Dans le cadre de notre cours de qualité des données, il nous a été demandé d’effectuer un comparatif entre deux jeux de données sur le climat. Ce comparatif a pour but de déterminer la capitale européenne dont les données de température sont inscrites dans le fichier Climat.xlsx.

## Analyse du sujet

Afin d’effectuer la réalisation du travail pratique, nous devrons tout d’abord tracer différentes courbes selon les mois afin d’avoir un visuel sur les données. Pour commencer, nous avons réalisé:

* Le calcul de l’écart type des températures mensuel

* Le calcul de la moyenne des températures sur chaques mois.

* Le calcul des température maximal et minimal sur chaque mois.

Ensuite, nous avons tracé une vue annuelle des températures en faisant en sorte de pouvoir afficher une vue sur 30 jours au travers d’une fenêtre glissante.Enfin, nous avons corrigé les données en erreur afin de supprimer les valeurs aberrantes et déterminer avec plus de précisions la ville européenne à deviner.

## Démarche de résolution
Pour réaliser ce tp nous avons travaillé sous l’outil Colab. Cet outil nous a permis de travailler tous en même temps sur le même fichier, cela fonctionne comme un google doc, c'est-à-dire que l’on peut voir les modifications de nos camarades en temps réel. Nous avons donc travaillé tous ensemble sur les questions en étant en vocal afin d’échanger sur la meilleure solution possible. Nous avons utilisé les documentations en ligne tel que la documentation de python panda et de matplotlib. Pour la dernière question, nous avons utilisé le site infoclimat.fr afin de retrouver la capitale correspondant au jeu de données.

## Captures d'écran

![](./screenshots/donnee_entrainement.png)

*Présentation des données*.

![](./screenshots/liste_mean.png)

*Présentation des moyennes sur l'année*.

![](./screenshots/ecart_type.png)

*Présentation des ecarts types sur l'années*.

![](./screenshots/extremums_temperature_mois.png)

*Présentation des extremums des températures selon les mois*.

![](./screenshots/extremums_temperature_annee.png)

*Présentation des extremums des températures selon les années*.

![](./screenshots/train_temperature_janvier.png)

*Présentation des températures sur le mois de janvier*.

![](./screenshots/temperature_annee_fenetre_glissante.png)

*Présentation de toutes les températures (avec fenêtre glissante)*.

![](./screenshots/erreur_ecart_type.png)

*Présentation des erreurs sur les ecarts types*.

![](./screenshots/erreur_extremums.png)

*Présentation des erreurs sur les extremums*.

![](./screenshots/erreurs_mois_annee.png)

*Présentation des erreurs sur les températures de l'années*.

## Conclusion

Après l’analyse des données, nous avons déterminé que la ville correspondant aux données météorologiques correspondait à la ville de Moscou ou Kiev.
